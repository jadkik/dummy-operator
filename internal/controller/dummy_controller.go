/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	interviewcomv1alpha1 "dummy-operator/api/v1alpha1"
)

var (
	reconcilerLog = ctrl.Log.WithName("reconciler")
)

// DummyReconciler reconciles a Dummy object
type DummyReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=interview.com,resources=dummies,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=interview.com,resources=dummies/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=interview.com,resources=dummies/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *DummyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	dummy := &interviewcomv1alpha1.Dummy{}
	err := r.Get(ctx, req.NamespacedName, dummy)
	if errors.IsNotFound(err) {
		reconcilerLog.Info("The dummy resource was deleted, nothing to do.", "NamespacedName", req.NamespacedName)
		return ctrl.Result{}, nil
	}
	if err != nil {
		return ctrl.Result{RequeueAfter: time.Duration(60) * time.Second}, fmt.Errorf("failed when reading dummy resource: %+v", err)
	}

	reconcilerLog.Info("Processing Dummy", "NamespacedName", req.NamespacedName, "Dummy.Namespace", dummy.Namespace, "Dummy.Name", dummy.Name, "Dummy.Message", dummy.Spec.Message)

	createPod := false
	if dummy.Status.PodName != "" {
		pod := &v1.Pod{}
		key := client.ObjectKey{
			Namespace: dummy.Namespace,
			Name:      dummy.Status.PodName,
		}
		err := r.Get(ctx, key, pod)
		if errors.IsNotFound(err) {
			reconcilerLog.Info("The dummy pod was deleted, will recreate...", "NamespacedName", req.NamespacedName)
			createPod = true
		} else if err != nil {
			return ctrl.Result{RequeueAfter: time.Duration(60) * time.Second}, fmt.Errorf("failed when reading dummy pod: %+v", err)
		} else {
			dummy.Status.PodStatus = string(pod.Status.Phase)
		}
	} else {
		createPod = true
	}

	if createPod {
		pod := &v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				GenerateName: fmt.Sprintf("%s-", dummy.Name),
				Namespace:    dummy.Namespace,
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name:  "nginx",
						Image: "docker.io/nginx:1.25-alpine",
					},
				},
			},
		}

		// TODO add finalizers to the custom resource / pods to react to deletions? We don't need them so far
		err = ctrl.SetControllerReference(dummy, pod, r.Scheme)
		if err != nil {
			return ctrl.Result{RequeueAfter: time.Duration(60) * time.Second}, fmt.Errorf("failed when setting owner reference on pod: %+v", err)
		}

		err = r.Create(ctx, pod)
		if err != nil {
			return ctrl.Result{RequeueAfter: time.Duration(60) * time.Second}, fmt.Errorf("failed when creating pod: %+v", err)
		}

		// TODO query by custom label instead of status field (cannot rely on owner reference alone https://github.com/kubernetes/client-go/issues/1085)
		dummy.Status.PodName = pod.Name

		dummy.Status.PodStatus = string(pod.Status.Phase)
	}

	dummy.Status.SpecEcho = dummy.Spec.Message
	err = r.Status().Update(ctx, dummy)
	if err != nil {
		return ctrl.Result{RequeueAfter: time.Duration(60) * time.Second}, fmt.Errorf("failed when updating dummy status: %+v", err)
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DummyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&interviewcomv1alpha1.Dummy{}).
		Owns(&v1.Pod{}).
		Complete(r)
}
