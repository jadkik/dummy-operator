# dummy-operator
Dummy operator manages `Dummy` custom resources and spins up `nginx` pods for those.

## Description

Every Dummy resource will be echoing its message and getting an `nginx` pod.
The pod's phase will be reflected in the custom resource's status.

## Running Instructions

First, let's rebuild to make sure we have things up to date:

```sh
# Update the generated code
make generate
# Generate the CRD and RBAC files containing the Kubernetes manifests
make manifests

# Start the kubernetes cluster
minikube start
```

### Running locally

To run the operator locally against the cluster:

```sh
make install run
```

### Running in the cluster

We need to build docker images to be able to run in the cluster.

To build, make sure you have `docker` set up, then run:

```sh
make docker-build
```

To push, you need to login to be able to push, then:

```sh
docker login
make docker-push
```

The docker image will be pushed on Docker Hub at [jadkik/dummy-operator](https://hub.docker.com/r/jadkik/dummy-operator).

If the image is already pushed, you can run this to deploy it to the cluster:

```sh
make deploy
```

To make sure it went fine, check those commands:

```sh
$ kubectl get deploy,pods -n dummy-operator-system
NAME                                                READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/dummy-operator-controller-manager   1/1     1            1           5m56s

NAME                                                     READY   STATUS    RESTARTS   AGE
pod/dummy-operator-controller-manager-554b5b4b7c-ts5st   2/2     Running   0          5m56s

$ kubectl logs deploy/dummy-operator-controller-manager -n dummy-operator-system
...
```

### Running a test and monitoring

In separate terminal windows, for monitoring:

```sh
# Watch the pods being created
kubectl get pods -w
# Watch the events to see relevant logs
kubectl get events -w
# Watch the dummies being created
kubectl get dummies -w -o jsonpath='{range .items[*]}{@.metadata.name}    {@.status.podName}    {@.status.podStatus}{"\n"}'
```

To create a test resource:

```sh
kubectl create -f ./config/samples/_v1alpha1_dummy.yaml
```

After running that command, you should see the pod being created and the log lines in the operator.

To delete that test resource:

```sh
kubectl delete dummies/dummy-sample
```

After running that command you should see the pods go to the `Terminating` state, then `Succeeded` state, then disappear.

The reconciler in its current implementation will still try to reconcile the pod and dummy resource while that happens.
This will lead to some errors until the reconciler reaches a steady state.

### Uninstall CRDs
To delete the CRDs from the cluster:

```sh
make uninstall
```

### Undeploy controller
UnDeploy the controller from the cluster:

```sh
make undeploy
```

## Contributing

Closed for contributions at the moment.

### How it works
This project aims to follow the Kubernetes [Operator pattern](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/).

It uses [Controllers](https://kubernetes.io/docs/concepts/architecture/controller/),
which provide a reconcile function responsible for synchronizing resources until the desired state is reached on the cluster.

### Test It Out
1. Install the CRDs into the cluster:

```sh
make install
```

2. Run your controller (this will run in the foreground, so switch to a new terminal if you want to leave it running):

```sh
make run
```

**NOTE:** You can also run this in one step by running: `make install run`

### Modifying the API definitions
If you are editing the API definitions, generate the manifests such as CRs or CRDs using:

```sh
make manifests
```

**NOTE:** Run `make --help` for more information on all potential `make` targets

More information can be found via the [Kubebuilder Documentation](https://book.kubebuilder.io/introduction.html)

## License

Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

